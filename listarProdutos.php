<div class="container">
	<div class="jumbotron">
		<h1>Calculadora de Descontos</h1>
		<p>Simule seus descontos de acordo com o tipo de produto, perfil de cliente e a quantidade de itens que serão comprados!</p> 
	</div>
	<div class="container-fluid text-justify">

		<?php
		if(isset($_GET['sucess'])) {
			echo "<div class='alert alert-success text-center' id='success.msg'>
			<a href='#' id='success.fechar' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Mensagem enviada com sucesso!</strong><br />
			<a href='index.php?p=welcome' class='btn btn-default'><span class='glyphicon glyphicon-hand-up'></span> Retornar para a página inicial</a>
			</div>";
		}
		?>

		<h1>Listagem de Produtos</h1>
		<p>Selecione um produto dentre os disponíveis na listagem abaixo:</p>

		<table class="table table-striped">
			<thead>
				<tr>
					<th>Id</th>
					<th>Nome do Produto</th>
					<th>Valor (R$)</th>
					<th>Ações</th>
				</tr>
			</thead>
			<tbody>
				<?php
					
					if(!isset($_SESSION['ui'])) {
						$_SESSION['ui'] = new InitUtils();
					}
					if(isset($_GET['prmid'])) {
						$id = 0;
						foreach ($_SESSION['ui']->produtos as $p) {
							if($p->id == $_GET['prmid']) {
								unset($_SESSION['ui']->produtos[$id]);
								$_SESSION['ui']->produtos = array_values($_SESSION['ui']->produtos);
							}
							$id += 1;
						}
					}
					foreach($_SESSION['ui']->produtos as $p) {
						echo "<tr>";
						echo "<td>" . $p->id . "</td>";
						echo "<td>" . $p->nome . "</td>";
						echo "<td>" . number_format($p->valor,2) . "</td>";
						echo "<td><a href='index.php?p=calcularDesconto&amp;pid=".$p->id."' class='btn btn-primary'><span class='glyphicon glyphicon-usd' title='Calcular Desconto do produto ".$p->nome."'></span></a><a href='index.php?p=listarProdutos&amp;prmid=".$p->id."' class='btn btn-danger' title='Remover Produto'><span class='glyphicon glyphicon-remove'></span></a></td>";
						echo "</tr>";
					}
				?>
			</tbody>
		</table>
		<ul class="pager">
			<li class="previous"><a href="index.php?p=welcome" id="listaprodutos.button.voltar">Voltar</a></li>
			<li class="next"><a href="index.php?p=listarProdutos&amp;rprod=1" id="listaprodutos.button.reiniciarlista">Reiniciar lista de Produtos</a></li>
		</ul>
	</div>
</div>