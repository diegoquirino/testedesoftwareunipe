<div class="container">
	<div class="jumbotron">
		<h1>Calculadora de Descontos</h1>
		<p>Simule seus descontos de acordo com o tipo de produto, perfil de cliente e a quantidade de itens que serão comprados!</p> 
	</div>
	<div class="container-fluid text-justify">

		<?php
		if(isset($_GET['sucess'])) {
			echo "<div class='alert alert-success text-center'>
			<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Operação realizada com sucesso!</strong><br />
			<a href='index.php?p=listarProdutos' class='btn btn-default'><span class='glyphicon glyphicon-hand-up'></span> Retornar para a Listagem de Produtos</a>
			</div>";
		}
		$id = 0;
		foreach ($_SESSION['ui']->produtos as $p) {
			if(isset($_GET['pid'])) {
				if($p->id == $_GET['pid']) {
					$_GET['produtoAtual'] = $p;
				}
			}
			$id += 1;
		}

		//IF DE CONTEUDO#1
		if(!isset($_GET['pid'])||!isset($_GET['produtoAtual'])) {
			echo "<div class='alert alert-danger text-center'>
			<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
			<strong>Você não selecionou um produto válido!</strong><br />
			<a href='index.php?p=listarProdutos' class='btn btn-default'><span class='glyphicon glyphicon-hand-up'></span> Retornar para a Listagem de Produtos</a>
			</div>";
		} else {
		//ELSE DE CONTEUDO#1	
			?>

			<h1>Cálculo do Desconto para o Produto</h1>
			<h2>Dados do Produto</h2>
			<p>Verifique os dados abaixo a fim de identificar se o produto foi selecionado corretamente: </p>
			<div class='well'>
				<p><strong>Código do Produto:</strong> <?php echo $_GET['produtoAtual']->id ?></p>
				<p><strong>Nome do Produto:</strong> <?php echo $_GET['produtoAtual']->nome ?></p>
				<p><strong>Descrição do Produto:</strong> <?php echo $_GET['produtoAtual']->descricao ?></p>
				<p><strong>Valor do Produto (R$):</strong> <?php echo number_format($_GET['produtoAtual']->valor,2) ?></p>
			</div>
			<h2>Formulário de informações complementares</h2>
			<p>Preencha os dados abaixo para proceder com o cálculo do desconto</p>
			<form role="form" action="index.php?p=descontoCalculado&amp;pid=<?php echo $_GET['produtoAtual']->id ?>&amp;sucess=1" method="post">
				<div class="form-group">
					<label for="tipoCliente">Tipo de cliente:</label>
					<select class="form-control" id="tipoCliente" name="tipoCliente">
						<option <?php if(!isset($_GET['tipoCliente'])||$_GET['tipoCliente']=="A") {echo "selected";} ?> >A</option>
						<option <?php if(isset($_GET['tipoCliente'])&&$_GET['tipoCliente']=="B") {echo "selected";} ?> >B</option>
						<option <?php if(isset($_GET['tipoCliente'])&&$_GET['tipoCliente']=="C") {echo "selected";} ?> >C</option>
						<option <?php if(isset($_GET['tipoCliente'])&&$_GET['tipoCliente']=="Outros") {echo "selected";} ?> >Outros</option>
					</select>
				</div>
				<div class="form-group">
					<label for="quantidade">Quantidade de itens a ser comprados:</label>
					<input type="text" class="form-control" id="quantidade" name="quantidade" placeholder="Quantos itens estima comprar? Ex: 145" />
				</div>
				<button type="submit" id="calculardesconto.button.calcular" class="btn btn-primary">Calcular Desconto!</button>
				<a href="index.php?p=listarProdutos" id="calculardesconto.button.cancelar" class="btn btn-default">Cancelar</a>
			</form>
			<?php 
		//ENDIF DE CONTEUDO#1
		} 
		?>

	</div>
</div>