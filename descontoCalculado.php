<div class="container">
	<div class="jumbotron">
		<h1>Calculadora de Descontos</h1>
		<p>Simule seus descontos de acordo com o tipo de produto, perfil de cliente e a quantidade de itens que serão comprados!</p> 
	</div>
	<div class="container-fluid text-justify">

		<?php
		if(isset($_GET['sucess'])) {
			echo "<div class='alert alert-success text-center' id='success.msg'>
			<a href='#' id='success.fechar' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Operação realizada com sucesso!</strong><br />
			<a href='index.php?p=listarProdutos' id='success.voltar' class='btn btn-default'><span class='glyphicon glyphicon-hand-up'></span> Retornar para a Listagem de Produtos</a>
			</div>";
		}
		$id = 0;
		foreach ($_SESSION['ui']->produtos as $p) {
			if(isset($_GET['pid'])) {
				if($p->id == $_GET['pid']) {
					$desc = new DescontoUtils();
					$_GET['produtoAtual'] = $p;
					$_GET['fator'] = $desc->calcularFatorDesconto($_POST['tipoCliente'], $_POST['quantidade']);
				}
			}
			$id += 1;
		}

		//IF DE CONTEUDO#1
		if(!isset($_GET['pid'])||!isset($_GET['produtoAtual'])) {
			echo "<div class='alert alert-danger text-center' id='danger.msg'>
			<a href='#' id='danger.fechar' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
			<strong>Você não selecionou um produto válido!</strong><br />
			<a href='index.php?p=listarProdutos' id='danger.voltar' class='btn btn-default'><span class='glyphicon glyphicon-hand-up'></span> Retornar para a Listagem de Produtos</a>
			</div>";
		} else {
		//ELSE DE CONTEUDO#1	
			?>

			<h1>Cálculo do Desconto para o Produto</h1>
			<div class="panel panel-info">
				<div class="panel-heading">Descontos Alcançados</div>
				<div class="panel-body">
					<p><strong>Tipo do Cliente:</strong> <?php echo $_POST['tipoCliente'] ?></p>
					<p><strong>Quantidade orçada:</strong> <?php echo $_POST['quantidade'] ?></p>
					<p><strong>Fator obtido:</strong> <?php echo $_GET['fator'] ?> (ou seja, <?php echo (1 - $_GET['fator']) * 100 ?>% de desconto)</p></p>
					<p><strong>Valor do produto com desconto (R$):</strong> <?php echo number_format($_GET['fator'] * $_GET['produtoAtual']->valor,2) ?></p></p>
				</div>
			</div>
			<h2>Dados do Produto</h2>
			<p>Verifique os dados originais do produto: </p>
			<div class='well'>
				<p><strong>Código do Produto:</strong> <?php echo $_GET['produtoAtual']->id ?></p>
				<p><strong>Nome do Produto:</strong> <?php echo $_GET['produtoAtual']->nome ?></p>
				<p><strong>Descrição do Produto:</strong> <?php echo $_GET['produtoAtual']->descricao ?></p>
				<p><strong>Valor do Produto (R$):</strong> <?php echo number_format($_GET['produtoAtual']->valor,2) ?></p>
			</div>
			
			<?php 
		//ENDIF DE CONTEUDO#1
		} 
		?>

		<ul class="pager">
			<li class="previous"><a href="index.php?p=calcularDesconto&amp;pid=<?php echo $_GET['produtoAtual']->id ?>&amp;tipoCliente=<?php echo $_POST['tipoCliente'] ?>" id="descontoCalculado.button.voltar">Voltar</a></li>
		</ul>

	</div>
</div>