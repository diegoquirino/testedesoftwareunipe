<?php

 class DescontoUtils {
	
	public function calcularFatorDesconto($cliente, $quant) {
		switch ($cliente) {
			case 'A':
				if($quant < 100) {
					return 0.9;
				} elseif ($quant < 1000) {
					return 0.85;
				} else {
					return 0.8;
				}
				break;
			case 'B':
				if($quant < 100) {
					return 0.95;
				} elseif ($quant < 1000) {
					return 0.9;
				} else {
					return 0.85;
				}
				break;
			case 'C':
				if($quant < 100) {
					return 1.00;
				} elseif ($quant < 1000) {
					return 0.95;
				} else {
					return 0.9;
				}
				break;
			default:
				return 1.0;
				break;
		}

	}

}

?>