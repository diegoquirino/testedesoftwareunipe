<?php

class InitUtils {
	
	var $produtos = array();

	public function __construct() {
		for($i=1; $i<=10; $i++) {
			$prod = new Produto($this->generateRandomString(rand(10,20)), rand(1000,5000));
			$prod->id = $i;
			$prod->descricao = $this->generateRandomString(rand(100,200));
			array_push($this->produtos, $prod);
		}
	}

	private function generateRandomString($length = 10) {
		$characters = 'abcdefghijklmn opqrstuvwxyz ABCDEFGHIJKLMN OPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

}

?>