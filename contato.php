<div class="container">
	<div class="jumbotron">
		<h1>Calculadora de Descontos</h1>
		<p>Simule seus descontos de acordo com o tipo de produto, perfil de cliente e a quantidade de itens que serão comprados!</p> 
	</div>
	<div class="container-fluid text-justify">

		<?php
		if(isset($_GET['sucess'])) {
			echo "<div class='alert alert-success text-center' id='success.msg'>
			<a href='#' id='sucess.fechar' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>".$_POST['tipoMsg']."-".$_POST['idade'].": Mensagem enviada com sucesso!</strong><br />
			<a href='index.php?p=welcome' id='success.voltar' class='btn btn-default'><span class='glyphicon glyphicon-hand-up'></span> Retornar para a Página Inicial</a>
			</div>";
		}
		?>

		<h1>Contato</h1>
		<p>Preencha as informações abaixo, e deixe-nos saber quais são as suas dúvidas!</p>

		<form role="form" action="index.php?p=contato&amp;sucess=1" method="post">
			<div class="form-group">
				<label for="nome">Nome:</label>
				<input type="text" class="form-control" id="nome" name="nome" placeholder="Seu Nome" />
			</div>

			<div class="form-group">
				<label for="email">Email:</label>
				<input type="email" class="form-control" id="email" name="email" placeholder="Seu@Email"/>
			</div>

			<div class="form-group">
				<label for="tipoMsg">Tipo de mensagem:</label>
				<label class="radio-inline"><input type="radio" value="duvida" name="tipoMsg" checked="true">Dúvida</input></label>
				<label class="radio-inline"><input type="radio" value="sugestao" name="tipoMsg">Sugestão</input></label>
				<label class="radio-inline"><input type="radio" value="reclamacao" name="tipoMsg">Reclamação</input></label>
			</div>
			
			<div class="form-group">
				<label for="idade">Idade:</label>
				<select class="selectpicker" id="idade" name="idade">
					<option>Menor que 18 anos</option>
					<option>Entre 18 e 60 anos</option>
					<option>Acima de 60 anos</option>
				</select>
			</div>

			<div class="form-group">
				<label for="mensagem">Escreva aqui a sua mensagem:</label><br />
				<textarea class="form-control" rows="10" id="mensagem" name="mensagem" placeholder="O que você tem a dizer?"></textarea>
			</div>

			<button type="submit" class="btn btn-primary" id="contato.button.enviarMensagem">Enviar sua mensagem</button>
			<a href="index.php?p=welcome" class="btn btn-default" id="contato.button.cancelarEnvio">Cancelar</a>
		</form>
		
	</div>
</div>