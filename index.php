<?php 
//Locale
setlocale(LC_ALL, "pt_BR");
// Inclusão de Classes
include 'classes/initutils.class.php';
include 'classes/produto.class.php';
include 'classes/descontoutils.class.php';
// Start da sessão
if(session_id() == '') {
  session_start();
}
if(isset($_GET['rprod'])){
  $_SESSION['ui'] = new InitUtils();
}
?>

<!DOCTYPE html>
<html lang="pt">
<head>
  <title>Calcular Desconto</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <link rel="stylesheet" href="./css/calcdesc.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="index.php?p=welcome">Calculadora de Descontos</a>
      </div>
      <div>
        <ul class="nav navbar-nav navbar-right">
          <li class="active"><a href="index.php?p=welcome" id="index.navlink.paginainicial">Página Inicial</a></li>
          <li><a href="index.php?p=listarProdutos" id="index.navlink.calculardesconto">Calcular Desconto</a></li>
          <li><a href="index.php?p=ajuda" id="index.navlink.ajuda">Ajuda</a></li> 
          <li><a href="index.php?p=contato" id="index.navlink.contato">Contato</a></li> 
        </ul>
      </div>
    </div>
  </nav>

  <?php
  if(!isset($_GET['p'])) {
    $_GET['p'] = 'welcome';
  } 
  if(file_exists($_GET['p'] . '.php')) {
    include $_GET['p'] . '.php';
  } else {
    echo "<div class='container text-center'><h1><span class='glyphicon glyphicon-floppy-remove'></span></h1><h1> Erro ao buscar a página solicitada</h1><h2>Código 404</h2><p>A página solicitada não existe no servidor: " .$_GET['p']. "</p><p><a href='index.php?p=welcome'>Clique aqui</a> para retornar para a página inicial.</p><p><a href='index.php?p=welcome' class='btn btn-primary' role='button'><span class='glyphicon glyphicon-hand-up'></span> Página Inicial</a></p></div>";
  }
  ?>

  <div class="footer text-center"><p>CalcDesc &copy; Todos os direitos reservados</p></div>

</body>
</html>
