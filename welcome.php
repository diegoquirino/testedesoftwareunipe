<div class="container">
  <div class="jumbotron">
    <h1>Calculadora de Descontos</h1>
    <p>Simule seus descontos para os seus produtos de acordo com o perfil de cliente e a quantidade de itens que serão comprados!</p> 
  </div>
  <div class="row">
    <div class="col-sm-4">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="text-center">Simulação</h3>
        </div>
        <div class="panel-body calcdesc-panel-height">
          <p>Já sabe qual o seu perfil de cliente e deseja fazer a sua simulação de desconto? <a href='index.php?p=listarProdutos'>Clique aqui</a> e faça sua simulação agora mesmo!</p>
        </div>
        <div class="panel-footer text-center plan">
          <a href="index.php?p=listarProdutos" class="btn btn-primary" role="button" id="welcome.button.calculardesconto">Calcular Desconto</a>
        </div>
      </div>
    </div>

    <div class="col-sm-4">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="text-center">Ajuda</h3>
        </div>
        <div class="panel-body calcdesc-panel-height">
          <p>Você ainda tem dúvidas de como calcular sua simulação de desconto? <a href='index.php?p=ajuda' >Clique aqui</a> agora mesmo e leia o conteúdo exclusivo que preparamos para você!</p>
        </div>
        <div class="panel-footer text-center plan">
          <a href="index.php?p=ajuda" class="btn btn-default" role="button" id="welcome.button.ajuda">Ajuda</a>
        </div>
      </div>
    </div>

    <div class="col-sm-4">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="text-center">Contato</h3>
        </div>
        <div class="panel-body calcdesc-panel-height">
          <p>Dúvidas, sugestões e críticas? <a href='index.php?p=contato'>Clique aqui</a> e nos mantenha informados agora mesmo!</p>
        </div>
        <div class="panel-footer text-center plan">
          <a href="index.php?p=contato" class="btn btn-default" role="button" id="welcome.button.contato">Contato</a>
        </div>
      </div>
    </div>

  </div>

</div>